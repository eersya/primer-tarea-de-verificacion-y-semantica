
-- Andoni Aarón Reyes Rodríguez

module PartePracticaSinTantosRodeos where

open import Agda.Builtin.String

-- tipo de datos para los booleanos

data 𝔹 : Set where
  True : 𝔹
  False : 𝔹

-- operaciones semánticas de expresiones booleanas

¬₀_ : 𝔹 → 𝔹

_∧₀_ : 𝔹 → 𝔹 → 𝔹

-- tipo de datos para los números enteros. Recordemos que los números enteros están construidos por la constante cero y las funciones antecesor y sucesor

data ℤ : Set where
  Zero : ℤ
  Suc : ℤ → ℤ
  Ant : ℤ → ℤ

-- operaciones semánticas de expresiones aritméticas

_+₀_ : ℤ → ℤ → ℤ

_−₀_ : ℤ → ℤ → ℤ

_×₀_ : ℤ → ℤ → ℤ

-- Expresión Aritmética

data Aₑ : Set where
  a : ℤ → Aₑ
  x : String → Aₑ
  _+₁_ : Aₑ → Aₑ → Aₑ
  _×₁_ : Aₑ → Aₑ → Aₑ
  _−₁_ : Aₑ → Aₑ → Aₑ

-- Expresión Booleana

data Bₑ : Set where
  bool : 𝔹 → Bₑ
  _=₁_ : Aₑ → Aₑ → Bₑ
  _<₁_ : Aₑ → Aₑ → Bₑ
  ¬₁_ : Bₑ → Bₑ
  _∧₁_ : Bₑ → Bₑ → Bₑ

-- Expresión

-- Variable en memoria. Celda

data Cell : Set where
  _↦_ : String → ℤ → Cell

-- Obtener la variable

--var : Cell → String
--var x ↦ y = x

-- Obtener el número guardado

--sto : Cell → ℤ
--sto x ↦ y = y

-- Memoria

--data Memo (C : Cell) : Set where
--[] : Memo C
--_::_ : C → Memo → Memo

--open import Agda.Builtin.List

--lookup : String → List Cell → ℤ
--lookup x σ = 

-- Función semántica para una expresión aritmética

--Α_⟦_⟧ : List Cell → Aₑ → ℤ
--A σ ⟦ a ⟧ = {!!}


-- Función semántica para una expresión booleana

--Β_⟦_⟧ : Memo → Bₑ → ℤ
