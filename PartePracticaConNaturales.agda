
-- Andoni Aarón Reyes Rodríguez

module PartePracticaConNaturales where

open import Agda.Builtin.Nat
open import Agda.Builtin.Bool

-- Tipo de datos para los booleanos

if_then_else_ : {A : Set} → Bool → A → A → A
if true then a else _ = a
if false then _ else b = b

_∧₀_ : Bool → Bool → Bool
false ∧₀ b = false
true ∧₀ b = b

¬₀_ : Bool → Bool
¬₀ false = true
¬₀ true = false

_=₀_ : Nat → Nat → Bool
zero =₀ zero = true
zero =₀ suc b = false
suc a =₀ zero = false
suc a =₀ suc b = a =₀ b

_≤₀_ : Nat → Nat → Bool
zero ≤₀ zero = true
zero ≤₀ suc b = true
suc a ≤₀ zero = false
suc a ≤₀ suc b = a ≤₀ b

-- Expresiones Aritméticas

data Aₑ : Set where
  n : Nat → Aₑ
  x : Nat → Aₑ
  _+₁_ : Aₑ → Aₑ → Aₑ
  _×₁_ : Aₑ → Aₑ → Aₑ
  _−₁_ : Aₑ → Aₑ → Aₑ

-- Expresiones Booleanas

data Bₑ : Set where
  bool : Bool → Bₑ
  _=₁_ : Aₑ → Aₑ → Bₑ
  _≤₁_ : Aₑ → Aₑ → Bₑ
  ¬₁_ : Bₑ → Bₑ
  _∧₁_ : Bₑ → Bₑ → Bₑ

-- Abstracción de una variable

data Cell : Set where
  _↦_ : Nat → Nat → Cell

-- Abstracción de una memoria

data Memo (C : Cell) : Set where
  [] : Memo C
  _::_ : Cell → Memo C → Memo C

overwrite : {C : Cell} → Cell → Memo C → Memo C
overwrite (x₁ ↦ z) [] = (x₁ ↦ z) :: []
overwrite (x₁ ↦ z₁) ((x₂ ↦ z₂) :: σ) = if (x₁ =₀ x₂) then ((x₁ ↦ z₁) :: σ) else (overwrite (x₁ ↦ z₁) σ)

-- Primera parte de la tarea práctica

data Maybe (A : Set) : Set where
  just : A → Maybe A
  nothing : Maybe A

-- Notemos que cuando la memoria está vacia la función devolverá zero(0), esto se puso así para no ocupar el tipo Maybe, que está definido arriba.
-- Cuando no exista la variable se devolvera zero
lookup : {C : Cell} → Nat → Memo C → Nat
lookup str [] = zero
lookup zero ((x₁ ↦ x₂) :: σ) = x₂
lookup (suc str) (x₁ :: σ) = lookup str σ

-- Función semántica para expresiones aritméticas
-- Α es \Alpha

Α_⟦_⟧ : {C : Cell} → Memo C → Aₑ → Nat
Α σ ⟦ n z ⟧ = z
Α σ ⟦ x str ⟧ = lookup str σ
Α σ ⟦ a₁ +₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) + (Α σ ⟦ a₂ ⟧)
Α σ ⟦ a₁ ×₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) * (Α σ ⟦ a₂ ⟧)
Α σ ⟦ a₁ −₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) - (Α σ ⟦ a₂ ⟧)

-- Función semántica para expresiones booleanas
-- Β es \Beta

Β_⟦_⟧ : {C : Cell} → Memo C → Bₑ → Bool
Β σ ⟦ bool c₁ ⟧ = c₁
Β σ ⟦ a₁ =₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) =₀ (Α σ ⟦ a₂ ⟧)
Β σ ⟦ a₁ ≤₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) ≤₀ (Α σ ⟦ a₂ ⟧) 
Β σ ⟦ ¬₁ b₁ ⟧ = ¬₀ (Β σ ⟦ b₁ ⟧)
Β σ ⟦ b₁ ∧₁ b₂ ⟧ = (Β σ ⟦ b₁ ⟧) ∧₀ (Β σ ⟦ b₂ ⟧)

-- Tercera y cuarta parte de la tarea práctica

data Sₑ : Set where
  _:=_ : Nat → Aₑ → Sₑ
  Skip : Sₑ
  _∘_ : Sₑ → Sₑ → Sₑ
  If_Then_Else_ : Bₑ → Sₑ → Sₑ → Sₑ
  While_Do_ : Bₑ → Sₑ → Sₑ
-- Cuarta parte de la tarea práctica
  For_:=_To_Do_ : Nat → Aₑ → Aₑ → Sₑ → Sₑ
  Repeat_Until_ : Sₑ → Bₑ → Sₑ

bigStep : {C : Cell} → Sₑ → Memo C → Memo C
bigStep (x₁ := a ) σ = overwrite (x₁ ↦ (Α σ ⟦ a ⟧)) σ
bigStep Skip σ = σ
bigStep (s₁ ∘ s₂) σ = bigStep s₂ (bigStep s₁ σ)
bigStep (If b Then s₁ Else s₂) σ = if (Β σ ⟦ b ⟧) then (bigStep s₁ σ) else (bigStep s₂ σ)
bigStep (While b Do s) σ = if (Β σ ⟦ b ⟧) then (bigStep (While b Do s) (bigStep s σ)) else σ
-- Semántica de paso grande para las nuevas estructuras que se agregraron
-- En el archivo ParteTeórica.pdf se da un breve explicación dea definición del  constructo para for
bigStep (For x₁ := a₁ To a₂ Do s) σ = bigStep (((x₁ := a₁) ∘ s) ∘ (While ((x x₁) ≤₁ a₂) Do s)) σ
bigStep (Repeat s Until b) σ = if (Β σ ⟦ b ⟧) then σ else (bigStep (s ∘ (Repeat s Until b)) σ)

-- Quinta parte de la tarea práctica

σ₅ = (2 ↦ 0) :: ( (1 ↦ 5) :: [])
S₅ = (2 := (n 1)) ∘ (  For 2 := (n 1) To (x 1) Do ( (2 := ((x 2) ×₁ (x 1))) ∘ (1 := ((x 1) −₁ (n 1))) )  )

quinta_parte = bigStep S₅ σ₅

-- quinta : bigStep (   ((suc (suc zero)) := (suc zero)) ∘ For (suc (suc ( suc zero))) To (suc zero) Do ( ( (suc (suc zero)) := ( (x (suc (suc zero))) × (x (suc zero)) )  );(suc zero) := ( (x (suc zero)) − (n (suc zero))  )  )   ) 
