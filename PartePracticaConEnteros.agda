
-- Andoni Aarón Reyes Rodríguez

module PartePracticaConEnteros where

open import Agda.Builtin.String
open import Agda.Builtin.Nat

-- Tipo de datos para los booleanos

data 𝔹 : Set where
  True : 𝔹
  False : 𝔹

¬₀_ : 𝔹 → 𝔹
¬₀ False = True
¬₀ True = False

_∧₀_ : 𝔹 → 𝔹 → 𝔹
False ∧₀ b₂ = False
True ∧₀ b₂ = b₂

if_then_else_ : {A : Set} → 𝔹 → A → A → A
if True then a else _ = a
if False then _ else b = b

-- Tipo de datos para los números enteros. Recordemos que los números enteros están construidos por la constante cero y las funciones antecesor y sucesor

data ℤ : Set where
  Zero : ℤ
  Suc : ℤ → ℤ
  Ant : ℤ → ℤ

_+₀_ : ℤ → ℤ → ℤ
Zero +₀ n₂ = n₂
Suc n₁ +₀ Zero = Suc n₁
Suc n₁ +₀ Suc n₂ = Suc (Suc (n₁ +₀ n₂) )
Suc n₁ +₀ Ant n₂ = n₁ +₀ n₂
Ant n₁ +₀ Zero = n₁
Ant n₁ +₀ Suc n₂ = n₁ +₀ n₂
Ant n₁ +₀ Ant n₂ = Ant ( Ant ( n₁ +₀ n₂) )

_−₀_ : ℤ → ℤ → ℤ
Zero −₀ n₂ = n₂
Suc n₁ −₀ Zero = Suc n₁
Suc n₁ −₀ Suc n₂ = n₁ −₀ n₂
Suc n₁ −₀ Ant n₂ = Suc ( Suc ( n₁ −₀ n₂ ) )
Ant n₁ −₀ Zero = Ant n₁
Ant n₁ −₀ Suc n₂ = Ant ( Ant ( n₁ −₀ n₂ ) )
Ant n₁ −₀ Ant n₂ = n₁ −₀ n₂

_×₀_ : ℤ → ℤ → ℤ
Zero ×₀ n₂ = Zero
Suc n₁ ×₀ Zero = Zero
Suc n₁ ×₀ Suc n₂ = Suc ( ( (n₁ ×₀ n₂) +₀ n₁ ) +₀ n₂)
Suc n₁ ×₀ Ant n₂ = Ant ( ( ( n₁ ×₀ n₂ ) −₀ n₁ ) +₀ n₂ )
Ant n₁ ×₀ Zero = Ant n₁
Ant n₁ ×₀ Suc n₂ = Ant ( ( ( n₁ ×₀ n₂ ) +₀ n₁ ) −₀ n₂ )
Ant n₁ ×₀ Ant n₂ = Suc ( ( ( n₁ ×₀ n₂ ) −₀ n₁ ) −₀ n₂ )

-- Errores en esta función
_=₀_ : ℤ → ℤ → 𝔹
Zero =₀ Zero = True
Zero =₀ Suc Zero = False
Zero =₀ Suc (Suc b) = True
Zero =₀ Suc (Ant b) = True
Zero =₀ Ant b = True
Suc a =₀ b = True
Ant a =₀ b = True

_≤₀_ : ℤ → ℤ → 𝔹
a₁ ≤₀ a₂ = True


-- Expresiones Aritméticas

data Aₑ : Set where
  n : ℤ → Aₑ
  x : Nat → Aₑ
  _+₁_ : Aₑ → Aₑ → Aₑ
  _×₁_ : Aₑ → Aₑ → Aₑ
  _−₁_ : Aₑ → Aₑ → Aₑ

-- Expresiones Booleanas

data Bₑ : Set where
  bool : 𝔹 → Bₑ
  _=₁_ : Aₑ → Aₑ → Bₑ
  _≤₁_ : Aₑ → Aₑ → Bₑ
  ¬₁_ : Bₑ → Bₑ
  _∧₁_ : Bₑ → Bₑ → Bₑ

-- Abstracción de una variable

data Cell : Set where
  _↦_ : Nat → ℤ → Cell

-- Abstracción de una memoria

data Memo (C : Cell) : Set where
  [] : Memo C
  _::_ : Cell → Memo C → Memo C

--overwrite : {C : Cell} → Cell → Memo C → Memo C
--overwrite (x₁ ↦ z) [] = (x₁ ↦ z) :: []
--overwrite (x₁ ↦ z₁) ((x₁ ↦ z₂) :: σ) = (x₁ ↦ z₁) :: σ
--overwrite (x₁ ↦ z₁) ((x₂ ↦ z₂) :: σ) = (x₂ ↦ z₂) :: (overwrite (x₁ ↦ z₁) σ)

-- Primera parte de la tarea práctica

data Maybe (A : Set) : Set where
  just : A → Maybe A
  nothing : Maybe A

-- Notemos que cuando la memoria está vacia la función devolverá Zero, eso se puso así para no ocupar el tipo Maybe, que está definido arriba.
lookup : {C : Cell} → Nat → Memo C → ℤ
lookup str [] = Zero
lookup zero ((x₁ ↦ x₂) :: σ) = x₂
lookup (suc str) (x₁ :: σ) = lookup str σ

-- Función semántica para expresiones aritméticas

Α_⟦_⟧ : {C : Cell} → Memo C → Aₑ → ℤ 
Α σ ⟦ n z ⟧ = z
Α σ ⟦ x str ⟧ = lookup str σ
Α σ ⟦ a₁ +₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) +₀ (Α σ ⟦ a₂ ⟧)
Α σ ⟦ a₁ ×₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) ×₀ (Α σ ⟦ a₂ ⟧)
Α σ ⟦ a₁ −₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) −₀ (Α σ ⟦ a₂ ⟧)

-- Función semántica para expresiones booleanas

Β_⟦_⟧ : {C : Cell} → Memo C → Bₑ → 𝔹
Β σ ⟦ bool c₁ ⟧ = c₁
Β σ ⟦ a₁ =₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) =₀ (Α σ ⟦ a₂ ⟧)
Β σ ⟦ a₁ ≤₁ a₂ ⟧ = (Α σ ⟦ a₁ ⟧) ≤₀ (Α σ ⟦ a₂ ⟧) 
Β σ ⟦ ¬₁ b₁ ⟧ = ¬₀ (Β σ ⟦ b₁ ⟧)
Β σ ⟦ b₁ ∧₁ b₂ ⟧ = (Β σ ⟦ b₁ ⟧) ∧₀ (Β σ ⟦ b₂ ⟧)

-- Tercera y cuarta parte de la tarea práctica

data Sₑ : Set where
  _:=_ : Nat → Aₑ → Sₑ
  skip : Sₑ
  _∘_ : Sₑ → Sₑ → Sₑ
  if₁_then_else_ : Bₑ → Sₑ → Sₑ → Sₑ
  while₁_do₁_ : Bₑ → Sₑ → Sₑ
-- Cuarta parte de la tarea práctica
  for_:=_to_Do_ : Nat → Aₑ → Aₑ → Sₑ → Sₑ
  repeat_until_ : Sₑ → Bₑ → Sₑ

bigStep : {C : Cell} → Sₑ → Memo C → Memo C
bigStep (x₁ := a ) σ = (x₁ ↦ (Α σ ⟦ a ⟧)) :: σ
bigStep skip σ = σ
bigStep (s₁ ∘ s₂) σ = bigStep s₂ (bigStep s₁ σ)
bigStep (if₁ b then s₁ else s₂) σ = if (Β σ ⟦ b ⟧) then (bigStep s₁ σ) else (bigStep s₂ σ)
bigStep (while₁ b do₁ s) σ = {!!}
bigStep (for x₁ := a₁ to a₂ Do s) σ = {!!}
bigStep (repeat s until b) σ = {!!}

-- Quinta parte de la tarea práctica

--bigStep ((2 := 1) ∘ (for x₁ := (Suc Zero) to 1 Do (2 := 2 * 1))) ((2 ↦ Zero) :: ((1 ↦ (Suc 0)) :: []))
